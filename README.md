Praca magisterska
=================

![logo](Dodatki/img/logo1.png)

[![Build Status](https://img.shields.io/jenkins/s/https/site.rafyco.pl/jenkins/Magisterka.svg?maxAge=2592000)](http://site.rafyco.pl/jenkins/job/Magisterka/)
[![Author](https://img.shields.io/badge/author-Rafa%C5%82%20Kobel-blue.svg)](http://rafyco.pl/)

> Opracowanie mechanizmu automatycznego tworzenia aplikacji typu line of business 

##Przygotowanie dokumentu

Aby zbudować pdf z pracą magisterską należy uruchomić skrypt ```make build```. Gotowy dokument pojawi się w katalogu out. 

Dodatkowe funkcje zdefiniowane na potrzeby edytowania dokumentu [dokumentacja](doc/myFunction.md)

##Wersja do pobrania

Najnowszą wersję skompilowanej pracy można pobrać ze strony [jenkins](http://site.rafyco.pl/jenkins/job/Magisterka/lastSuccessfulBuild/artifact/out/Praca_production.pdf)

W przypadku braku dostępu należy skontaktować się z autorem.

##Otrzymane pliki

 * ![praca][pdf] [Praca magisterska](http://site.rafyco.pl/jenkins/job/Magisterka/lastSuccessfulBuild/artifact/out/Praca_production.pdf)
 * ![praca][pdf] [Praca magisterska - wersja robocza](http://site.rafyco.pl/jenkins/job/Magisterka/lastSuccessfulBuild/artifact/out/Praca_debug.pdf)
 * ![api][pdf] [Dokumentacja biblioteki Ajango](http://site.rafyco.pl/jenkins/job/Magisterka/lastSuccessfulBuild/artifact/out/Ajango.pdf)

##Autor

Rafał Kobel <r.kobel@stud.elka.pw.edu.pl>


[pdf]: https://cdn1.iconfinder.com/data/icons/CrystalClear/16x16/mimetypes/pdf.png

[pre]: https://cdn3.iconfinder.com/data/icons/UltimateGnome/16x16/mimetypes/application-vnd.ms-powerpoint.png
