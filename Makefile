#############################################################################
# Copyright (C) 2015  Rafał Kobel <rafyco1@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Rafal Kobel <rafyco1@gmail.com>
# Project: magisterka
# Description: Tresc mojej pracy magisterskiej
#############################################################################

TEX                    = pdflatex
SED                    = sed
DEL_FILE               = rm -f
DEL_DIR                = rm -rf
MOVE                   = mv -f
CHK_DIR_EXISTS         = test -d
MKDIR                  = mkdir -p
MD5                    = md5sum
COPY                   = cp
MV                     = mv
EPYDOC                 = epydoc.py

TEXFLAGS               = -halt-on-error -interaction=nonstopmode

OBJECTS_DIR            = out/

FILE_TARGET_DEBUG      = Praca_develop.pdf
TARGET_DEBUG           = $(OBJECTS_DIR)/$(FILE_TARGET_DEBUG)
SOURCE_DEBUG           = Praca_develop.tex

FILE_TARGET_PRODUCTION = Praca_production.pdf
TARGET_PRODUCTION      = $(OBJECTS_DIR)/$(FILE_TARGET_PRODUCTION)
SOURCE_PRODUCTION      = Praca_production.tex

FILE_TARGET_TEST       = testy.pdf
TARGET_TEST            = $(OBJECTS_DIR)/$(FILE_TARGET_TEST)
SOURCE_TEST            = testy.tex


TARGET                 = $(TARGET_DEBUG) $(TARGET_PRODUCTION)
SOURCE                 = lib/*.* \
                         Rozdzial01/*.* \
                         Rozdzial02/*.* \
                         Rozdzial02/*/*.* \
                         Rozdzial03/*.* \
                         Rozdzial04/*.*

MD5_FILE               = Praca.md5


####### Build rules

all: build

images:
	@mpost Rozdzial02/images/uml1.mp
	@mv uml1.1 Rozdzial02/images/
	
clean:
	@$(DEL_FILE) *.aux *.lof *.lol *.lot *.out *.toc *.log *.tdo *.pdf *.hst *.ver Rozdzial*/images/*.1
	@echo "Usuniecie plikow kompilacyjnych"

build: build-debug build-production build-tests clean

build-debug: images $(TARGET_DEBUG)

debug: build-debug clean

build-production: images $(TARGET_PRODUCTION)

build-tests: $(TARGET_TEST)

build-documentation: 
	@$(EPYDOC) -v -o doc --pdf --name "Ajango Documentation" Dodatki/Projekty/ajango/ajango

remore:
	./Dodatki/mgr_compilation.sh

mgr: build-production build-documentation clean

$(TARGET_DEBUG): $(SOURCE_DEBUG) $(SOURCE)
	@$(MKDIR) $(OBJECTS_DIR)
	$(TEX) $(TEXFLAGS) $(SOURCE_DEBUG)
	$(TEX) $(TEXFLAGS) $(SOURCE_DEBUG)
	$(MV) $(FILE_TARGET_DEBUG) $(OBJECTS_DIR)/$(FILE_TARGET_DEBUG)
	$(MD5) $(TARGET_DEBUG) >> $(OBJECTS_DIR)/$(MD5_FILE)

$(TARGET_PRODUCTION): $(SOURCE_PRODUCTION) $(SOURCE)
	@$(MKDIR) $(OBJECTS_DIR)
	$(TEX) $(TEXFLAGS) $(SOURCE_PRODUCTION)
	$(TEX) $(TEXFLAGS) $(SOURCE_PRODUCTION)
	$(MV) $(FILE_TARGET_PRODUCTION) $(OBJECTS_DIR)/$(FILE_TARGET_PRODUCTION)
	$(MD5) $(TARGET_PRODUCTION) >> $(OBJECTS_DIR)/$(MD5_FILE)

$(TARGET_TEST): $(SOURCE_TEST) $(SOURCE_TEST)
	@$(MKDIR) $(OBJECTS_DIR)
	$(TEX) $(TEXFLAGS) $(SOURCE_TEST)
	$(MV) $(FILE_TARGET_TEST) $(OBJECTS_DIR)/$(FILE_TARGET_TEST)
	$(MD5) $(TARGET_TEST) >> $(OBJECTS_DIR)/$(MD5_FILE)

remove: clean
	@$(DEL_DIR) out
	@echo "Usuniecie plikow pdf"

sign: $(TARGET_PRODUCTION) $(TARGET_DEBUG) clean
	gpg --output $(TARGET_PRODUCTION).sig $(TARGET) 
	gpg --output $(TARGET_DEBUG).sig $(TARGET)
