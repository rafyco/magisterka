# Funkcje napisane na potrzeby dokumentu

```\todoadd``` - Elementy do wykonania w pozniejszym czasie.
```\todotip``` - Wskazówka do elementu do dodania
```\todoerr``` - Niepoprawny tekst z jakiegoś powodu
```\addref``` - Miejsce okreslające dodanie odnośnika
```\rewrite``` - Sugestia żeby coś przepisać
```\info``` - Wiadomość dla sprawdzającego

Polecenia te można użyć do wskazywania miejsc poprawek, oraz informowania o ważnych zmianach które należy przeprowadzić w przyszłości w tekście.