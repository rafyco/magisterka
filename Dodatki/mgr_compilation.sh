#!/bin/bash

# Skrypt do zdalnego kompilowania programu na zewnętrznym komputerze
# Do poprawnego działania wymaga zarejestrowanego klucza ssh lub każdorazowego podawania hasła 
# podczas łączenia.

# Ścieżka do ściąganego pliku na komputerze
LOCAL_PATH=$HOME/programs/magisterka/out
# Host zdalnego oprogramowania
HOST_ADD=unknown
# Ścieżka do pliku na zdalnym komputerze
HOST_PATH=\$HOME/programs/magisterka
# Program do uruchomienia ściągniętego pliku (opcjonalne)
OPEN_PROG=
#Odkomentuj w celu zbudowania jedynie wersji debugowej
#OPTION=-debug


if [[ -f $HOME/.mgr_compilation ]]; then
	. $HOME/.mgr_compilation
fi

if [[ $HOST_ADD = unknown ]]; then
	echo "
	Skrypt do poprawnego działania wymaga pliku $HOME/.mgr_compilation
	Proszę zedytować ten plik.
	"
	echo "\
# Ścieżka do ściąganego pliku na komputerze
LOCAL_PATH=$HOME/programs/magisterka/out
# Host zdalnego oprogramowania
HOST_ADD=unknown
# Ścieżka do pliku na zdalnym komputerze
HOST_PATH=\$HOME/programs/magisterka
# Program do uruchomienia ściągniętego pliku (opcjonalne)
OPEN_PROG=
#Odkomentuj w celu zbudowania jedynie wersji debugowej
#OPTION=-debug	
	
	
	" >> $HOME/.mgr_compilation
	exit 1
fi

mkdir -p $LOCAL_PATH

cd $LOCAL_PATH

ssh $HOST_ADD "cd $HOST_PATH; make remove; make build$OPTION"
ERR=$?

if [[ $ERR != 0 ]]; then
	exit $ERR
fi

echo Usuwanie starych plikow
rm *.pdf *.md5

FILE_SCP='*'
if [[ $OPTION = '-debug' ]]; then
	FILE_SCP='Praca_develop.pdf'
fi
scp $HOST_ADD:$HOST_PATH/out/$FILE_SCP .

if [[ $OPEN_PROG != "" ]]; then
	"$OPEN_PROG" Praca_develop.pdf
fi

