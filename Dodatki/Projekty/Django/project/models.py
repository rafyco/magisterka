from django.db import models
from django.utils import timezone

class Man(models.Model):
    name = models.CharField(max_length=200)
    created_date = models.DateTimeField(
            default=timezone.now)
    birth_date = models.DateTimeField(
            blank=True, null=True)

    def born(self):
        self.birth_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title
        