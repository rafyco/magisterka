#! /bin/bash

AJANGO_DIR=./ajango
AJANGO_TAG=ajango_tag
AJANGO_PORT=8000
AJANGO_SITES="blog/nowa blog/pracownicy blog/presents/?id=2 blog/new_prc"

DJANGO_DIR=./django-test
DJANGO_TAG=django_tag
DJANGO_PORT=8001
DJANGO_SITES="index list position?id=2 input"

echo "TESTY SYSTEMU AJANGO"

cd $AJANGO_DIR
screen -dmS $AJANGO_TAG make run

for site in $AJANGO_SITES; do
	echo "
-------- Strona: http://localhost:$AJANGO_PORT/$site -----------
"
	#curl -w "%{time_connect}:%{time_starttransfer}:%{time_total}" -o /dev/null -s http://localhost:$AJANGO_PORT/$site
	curl -w "
	       time connect: %{time_connect}
	time start transfer: %{time_starttransfer}
	         total time: %{time_total}
	" -o /dev/null -s http://localhost:$AJANGO_PORT/$site
	echo "
-----------------------------------------------

"
done
screen -X -S $AJANGO_TAG kill 
cd -

sleep 30

echo "TESTY SYSTEMU DJANGO"

cd $DJANGO_DIR
screen -dmS $DJANGO_TAG make run

for site in $DJANGO_SITES; do
	echo "
-------- Strona: http://localhost:$AJANGO_PORT/$site -----------
"
	curl -w "
	       time connect: %{time_connect}
	time start transfer: %{time_starttransfer}
	         total time: %{time_total}
	" -o /dev/null -s http://localhost:$DJANGO_PORT/$site
	echo "
-----------------------------------------------

"
done
screen -X -S $DJANGO_TAG kill 
